const initialState = {
  searchField: "",
  articles:[],
  store_menu:["Parallel Deals"],
  description_menu:["Original", "Simple", "Images Only"],
  sku_preference:["ShopMaster SKU", "Source Product ID"],
  sub_settings: "general",
  pricing_rules:["ON", "OFF"],
  pricing_formula:["Simple", "Advanced", "Formula"],
  pricing_rules_selected:"",
  pricing_formula_selected:"",
  country_list_shipfrom: ["CN", "USA", "EU", "AU", "FR", "GWTR", "RU", "ES", "CZ", "AE", "BR", "PL", "JP", "SA", "NL", "HK", "UK"],
  token:[],
  country_list_shipto: ["Australia", "New Zealand", "United States", "United Kingdom", "Canada"],
  selected_country_shipfrom: "",
  selected_country_shipto: "",
  shipping_methods:[],
  selected_shipping_method:"",
  import_options:["Import by Category", "Import by Product ID", "Import by URL"],
  selected_import_option:"",
  categories_level1: [],
  categories_level2: [],
  categories_level3: [],
  selected_category_level2:"",
  selected_category_level3:"",
  selected_category_level4:"",
  selected_category_level5:"",
  selected_category_level6:"",
  categories_level4: [],
  categories_level5: [],
  categories_level6: [],
  imported_products:[],
  imported_products_filter:[],
}

export const rootReducer = (state=initialState, action={}) => {
  switch(action.type){
    case "CHANGE_SEARCH_FIELD":
      return Object.assign({}, state, {searchField:action.payload})
      case "CHANGE_ARTICLES":
        return Object.assign({}, state, {articles:action.payload})
      case "CHANGE_SUBSETTINGS":
        return Object.assign({}, state, {sub_settings:action.payload})
      case "CHANGE_PRICING_RULE":
        return Object.assign({}, state, {pricing_rules_selected:action.payload})
        case "CHANGE_PRICING_fORMULA":
          return Object.assign({}, state, {pricing_formula_selected:action.payload})
        case "CHANGE_COUNTRY_LIST":
          return Object.assign({}, state, {pricing_formula_selected:action.payload})
      case "CHANGE_TOKEN":
          return Object.assign({}, state, {token:action.payload})
      case "CHANGE_SELECTED_COUNTRY_SHIPFROM":
          return Object.assign({}, state, {selected_country_shipfrom:action.payload})
      case "CHANGE_SELECTED_COUNTRY_SHIPTO":
          return Object.assign({}, state, {selected_country_shipto:action.payload})
      case "CHANGE_SELECTED_SHIPPING_METHOD":
          return Object.assign({}, state, {selected_shipping_method:action.payload})
      case "CHANGE_SHIPPING_METHODS":
          return Object.assign({}, state, {shipping_methods:action.payload})
      case "CHANGE_IMPORT_OPTION":
          return Object.assign({}, state, {selected_import_option:action.payload})
      case "CHANGE_CATEGORIES_LEVEL1":
          return Object.assign({}, state, {categories_level1:action.payload})
      case "CHANGE_CATEGORIES_LEVEL2":
          return Object.assign({}, state, {categories_level2:action.payload})
      case "CHANGE_CATEGORIES_LEVEL3":
          return Object.assign({}, state, {categories_level3:action.payload})
      case "CHANGE_SELECTED_CATEGORY_LEVEL2":
          return Object.assign({}, state, {selected_category_level2:action.payload})
      case "CHANGE_SELECTED_CATEGORY_LEVEL3":
          return Object.assign({}, state, {selected_category_level3:action.payload})
      case "CHANGE_CATEGORIES_LEVEL4":
          return Object.assign({}, state, {categories_level4:action.payload})
      case "CHANGE_CATEGORIES_LEVEL5":
          return Object.assign({}, state, {categories_level5:action.payload})
      case "CHANGE_CATEGORIES_LEVEL6":
          return Object.assign({}, state, {categories_level6:action.payload})
      case "CHANGE_SELECTED_CATEGORY_LEVEL4":
          return Object.assign({}, state, {selected_category_level4:action.payload})
      case "CHANGE_SELECTED_CATEGORY_LEVEL5":
          return Object.assign({}, state, {selected_category_level5:action.payload})
      case "CHANGE_SELECTED_CATEGORY_LEVEL6":
          return Object.assign({}, state, {selected_category_level6:action.payload})
      case "CHANGE_IMPORTED_PRODUCTS":
          return Object.assign({}, state, {imported_products:action.payload})
      case "CHANGE_IMPORTED_PRODUCTS_FILTER":
          return Object.assign({}, state, {imported_products_filter:action.payload})
    default:
      return state
  }
}
