export const setSearch = (text) => ({
  type: "CHANGE_SEARCH_FIELD",
  payload: text
}
)

export const setArticles = (data) => ({
  type: "CHANGE_ARTICLES",
  payload: data
}
)

export const setSubsettings = (data) => ({
  type: "CHANGE_SUBSETTINGS",
  payload: data
}
)

export const setPricingrule = (data) => ({
  type: "CHANGE_PRICING_RULE",
  payload: data
}
)

export const setPricingformula = (data) => ({
  type: "CHANGE_PRICING_FORMULA",
  payload: data
}
)

export const setCountrylist = (data) => ({
  type: "CHANGE_COUNTRY_LIST",
  payload: data
}
)

export const setToken = (data) => ({
  type: "CHANGE_TOKEN",
  payload: data
}
)

export const setSelectedCountryShipFrom = (data) => ({
  type: "CHANGE_SELECTED_COUNTRY_SHIPFROM",
  payload: data
}
)

export const setSelectedCountryShipTo = (data) => ({
  type: "CHANGE_SELECTED_COUNTRY_SHIPTO",
  payload: data
}
)

export const setSelectedShippingMethod = (data) => ({
  type: "CHANGE_SELECTED_SHIPPING_METHOD",
  payload: data
}
)

export const setShippingMethods = (data) => ({
  type: "CHANGE_SHIPPING_METHODS",
  payload: data
}
)

export const setSelectedImportOption = (data) => ({
  type: "CHANGE_IMPORT_OPTION",
  payload: data
}
)

export const setCategoriesLevel1 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL1",
  payload: data
}
)

export const setCategoriesLevel2 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL2",
  payload: data
}
)

export const setCategoriesLevel3 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL3",
  payload: data
}
)

export const setCategoriesLevel4 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL4",
  payload: data
}
)

export const setCategoriesLevel5 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL5",
  payload: data
}
)

export const setCategoriesLevel6 = (data) => ({
  type: "CHANGE_CATEGORIES_LEVEL6",
  payload: data
}
)

export const setSelectedCategoryLevel2 = (data) => ({
  type: "CHANGE_SELECTED_CATEGORY_LEVEL2",
  payload: data
}
)

export const setSelectedCategoryLevel3 = (data) => ({
  type: "CHANGE_SELECTED_CATEGORY_LEVEL3",
  payload: data
}
)

export const setSelectedCategoryLevel4 = (data) => ({
  type: "CHANGE_SELECTED_CATEGORY_LEVEL4",
  payload: data
}
)

export const setSelectedCategoryLevel5 = (data) => ({
  type: "CHANGE_SELECTED_CATEGORY_LEVEL5",
  payload: data
}
)

export const setSelectedCategoryLevel6 = (data) => ({
  type: "CHANGE_SELECTED_CATEGORY_LEVEL6",
  payload: data
}
)

export const setImportedProducts = (data) => ({
  type: "CHANGE_IMPORTED_PRODUCTS",
  payload: data
}
)

export const setImportedProductsFilter = (data) => ({
  type: "CHANGE_IMPORTED_PRODUCTS_FILTER",
  payload: data
}
)
