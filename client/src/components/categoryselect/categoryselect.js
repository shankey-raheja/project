import React, {useEffect} from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {useSelector, useDispatch} from "react-redux"
import {setSearch, setArticles,setSubsettings, setToken, setCountrylist,setSelectedCountryShipFrom,setSelectedCountryShipTo,setSelectedShippingMethod, setShippingMethods, setSelectedCategoryLevel2, setSelectedCategoryLevel3, setSelectedCategoryLevel4,setSelectedCategoryLevel5,setSelectedCategoryLevel6} from "../../actions"

export default function BasicSelect({label, menu, type}) {
  const [age, setAge] = React.useState('');
  const selected_country_shipfrom = useSelector(state => state.selected_country_shipfrom)
  const selected_country_shipto = useSelector(state => state.selected_country_shipto)
  const selected_shipping_method = useSelector(state => state.selected_shipping_method)
  const country_list_shipfrom = useSelector(state => state.country_list_shipfrom)
  const country_list_shipto = useSelector(state => state.country_list_shipto)
  const shipping_methods = useSelector(state => state.shipping_methods)
  const selected_category_level2 = useSelector(state => state.selected_category_level2)
  const selected_category_level3 = useSelector(state => state.selected_category_level3)
  const selected_category_level4 = useSelector(state => state.selected_category_level4)
  const selected_category_level5 = useSelector(state => state.selected_category_level5)
  const selected_category_level6 = useSelector(state => state.selected_category_level6)
  const dispatch = useDispatch()
  const handleChange = (event) => {
    setAge(event.target.value);
    if(type === "shipfrom"){
      dispatch(setSelectedCountryShipFrom(event.target.value))
    }
    if(type === "shipto"){
      dispatch(setSelectedCountryShipTo(event.target.value))
    }
    if(type === "shippingmethod"){
      dispatch(setSelectedShippingMethod(event.target.value))
    }
    if(type === "CATLEVEL2"){
      dispatch(setSelectedCategoryLevel2(event.target.value))
    }
    if(type === "CATLEVEL3"){
      dispatch(setSelectedCategoryLevel3(event.target.value))
    }
    if(type === "CATLEVEL4"){
      dispatch(setSelectedCategoryLevel4(event.target.value))
    }
    if(type === "CATLEVEL5"){
      dispatch(setSelectedCategoryLevel5(event.target.value))
    }
    if(type === "CATLEVEL6"){
      dispatch(setSelectedCategoryLevel6(event.target.value))
    }
  };
  useEffect(() => {
      dispatch(setShippingMethods([]))
      dispatch(setSelectedShippingMethod(""))
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "USA"){
        dispatch(setShippingMethods(["Banggood Express", "Air Parcel Register", "Express Shipping"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["Standard Mail Register", "AU Direct Mail", "Priority Mail", "Express Shipping"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["New Zealand Direct Mail", "Air Parcel Register"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "UK"){
        dispatch(setShippingMethods(["Banggood Express"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "USA"){
        dispatch(setShippingMethods(["US Standard Shipping","US Express Shipping"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "CANADA"){
        dispatch(setShippingMethods(["US International Shipping"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["US International Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["AU Standard Shipping", "AU Express Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["NZ Standard Shipping", "NZ Tracked Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["NZ Standard Shipping", "NZ Tracked Shipping"]))
      }
      if(selected_country_shipfrom === "UK" && selected_country_shipto === "UK"){
        dispatch(setShippingMethods(["UK Tracked Shipping", "UK Standard Shipping"]))
      }

  }, [selected_country_shipfrom,selected_country_shipto])
  console.log(selected_category_level2, selected_category_level3, selected_category_level4, selected_category_level5, selected_category_level6)
  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        >
        {
          menu.map(item => {
              return(
                <MenuItem value={item.cat_id}>{item.cat_name}</MenuItem>
              )
        })
      }

        </Select>
      </FormControl>
    </Box>
  );
}
