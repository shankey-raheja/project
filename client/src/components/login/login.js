import React, {useState, useEffect} from "react"
import "./login.css"
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import LoginIcon from '@mui/icons-material/Login';
import Button from '@mui/material/Button';

function Login({handleLogin, handleAdminUser}) {
    const [orders, setOrders] = useState("")
  return (
    <div >
    <div className="login">
    <div></div>
    <Card className="card" style={{ marginTop:"40%", marginBottom:"50%", height:"380px"}}>
    <form className="form">
              <Typography variant="h5" component="div" gutterBottom>Sign In</Typography>
              <div className="form-group">
                  <div className="input">
                  <label className="label">Username</label>
                  </div>
                  <div className="input">
                  <input type="email" className="form-control" placeholder="Enter email" height="40px" onChange={(event) => handleAdminUser(event, "user")}/>
                  </div>
              </div>

              <div className="form-group">
                  <div className="input">
                  <label className="label">Password</label>
                  </div>
                  <div className="input">
                  <input type="password" className="form-control" placeholder="Enter password" onChange={(event) => handleAdminUser(event, "pass")} />
                  </div>
              </div>
              <div className="submit">
              <Button variant="contained" endIcon={<LoginIcon />} onClick={(event) => handleLogin(event)}>Sign in</Button>
              </div>
          </form>
          <div></div>
          </Card>
    </div>
    </div>

    )
}

export default Login;
