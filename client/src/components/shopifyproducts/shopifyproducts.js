import React, {useState, useEffect} from "react"

function ShopifyProducts() {
    const [products, setProducts] = useState("")

    useEffect(() => {
      const data = async () => {
        // await fetch("https://thingproxy.freeboard.io/fetch/https://goslash-au.myshopify.com/admin/api/2021-07/orders.json?limit=250",
        await fetch(`http://localhost:3001/shopifyproducts`).then(response => response.json()).then(response => setProducts(response))

        // await fetch("https://cors-anywhere.herokuapp.com/https://api.banggood.com/getAccessToken?app_id=bg614e8e254eca7&app_secret=5e76df83d25c08505022011c3d0f75f9", {headers:{"access-control-allow-origin" : "*"}}).then(response => response.json()).then(response => console.log(response))
      }
      data()
    }, [])


  console.log("products", products)

  return (
        <div>
        Hello World
        </div>
        )
}

export default ShopifyProducts;
