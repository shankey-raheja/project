import React, {useEffect} from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {useSelector, useDispatch} from "react-redux"
import {setSearch, setArticles,setSubsettings, setToken, setCountrylist,setSelectedCountryShipFrom,setSelectedCountryShipTo,setSelectedShippingMethod, setShippingMethods} from "../../actions"

export default function BasicSelect({label, menu, type}) {
  const [age, setAge] = React.useState('');
  const selected_country_shipfrom = useSelector(state => state.selected_country_shipfrom)
  const selected_country_shipto = useSelector(state => state.selected_country_shipto)
  const selected_shipping_method = useSelector(state => state.selected_shipping_method)
  const country_list_shipfrom = useSelector(state => state.country_list_shipfrom)
  const country_list_shipto = useSelector(state => state.country_list_shipto)
  const shipping_methods = useSelector(state => state.shipping_methods)
  const dispatch = useDispatch()
  const handleChange = (event) => {
    setAge(event.target.value);
    if(type === "shipfrom"){
      dispatch(setSelectedCountryShipFrom(event.target.value))
    }
    if(type === "shipto"){
      dispatch(setSelectedCountryShipTo(event.target.value))
    }
    if(type === "shippingmethod"){
      dispatch(setSelectedShippingMethod(event.target.value))
    }
  };
  useEffect(() => {
      dispatch(setShippingMethods([]))
      dispatch(setSelectedShippingMethod(""))
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "USA"){
        dispatch(setShippingMethods(["Banggood Express", "Air Parcel Register", "Express Shipping"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["Standard Mail Register", "AU Direct Mail", "Priority Mail", "Express Shipping"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["New Zealand Direct Mail", "Air Parcel Register"]))
      }
      if(selected_country_shipfrom === "CN" && selected_country_shipto === "UK"){
        dispatch(setShippingMethods(["Banggood Express"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "USA"){
        dispatch(setShippingMethods(["US Standard Shipping","US Express Shipping"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "CANADA"){
        dispatch(setShippingMethods(["US International Shipping"]))
      }
      if(selected_country_shipfrom === "USA" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["US International Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "AU"){
        dispatch(setShippingMethods(["AU Standard Shipping", "AU Express Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["NZ Standard Shipping", "NZ Tracked Shipping"]))
      }
      if(selected_country_shipfrom === "AU" && selected_country_shipto === "NZ"){
        dispatch(setShippingMethods(["NZ Standard Shipping", "NZ Tracked Shipping"]))
      }
      if(selected_country_shipfrom === "UK" && selected_country_shipto === "UK"){
        dispatch(setShippingMethods(["UK Tracked Shipping", "UK Standard Shipping"]))
      }

  }, [selected_country_shipfrom,selected_country_shipto])
  console.log(selected_country_shipfrom, selected_country_shipto, shipping_methods, selected_shipping_method)
  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        >
        {
          menu.map(item => {
              return(
                <MenuItem value={item}>{item}</MenuItem>
              )
        })
      }

        </Select>
      </FormControl>
    </Box>
  );
}
