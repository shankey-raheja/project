import React, {useState, useEffect} from "react"

import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import LoginIcon from '@mui/icons-material/Login';
import Button from '@mui/material/Button';
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import BasicSelect from "../select/select"
import {useSelector, useDispatch} from "react-redux"
import {setSearch, setArticles,setSubsettings, setToken, setCountrylist,setShippingMethods} from "../../actions"
import ToggleButtons from "../togglebutton/togglebutton"

function GeneralSettings() {
    const [orders, setOrders] = useState("")
    const sku_preference = useSelector(state => state.sku_preference)
    const store_menu = useSelector(state => state.store_menu)
    const description_menu = useSelector(state => state.description_menu)
    const pricing_rules = useSelector(state => state.pricing_rules)
    const pricing_formula = useSelector(state => state.pricing_formula)
    const pricing_rules_selected = useSelector(state => state.pricing_rules_selected)
    const country_list_shipfrom = useSelector(state => state.country_list_shipfrom)
    const country_list_shipto = useSelector(state => state.country_list_shipto)
    const shipping_methods = useSelector(state => state.shipping_methods)
    const dispatch = useDispatch()
    const token = useSelector(state => state.token)


  console.log(country_list_shipfrom)
  console.log(token)



  console.log(shipping_methods)
  return (
    <>
    <Grid item xs={12} sm={12} lg={12}>
      <Typography variant="h5">General Settings</Typography>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Default Store</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Select store" menu={store_menu}/>
    </Grid>
    <Divider/>
    <Grid item xs={5} sm={5} lg={5}>
      <div>SKU Preference</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Select store" menu={sku_preference}/>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Description Preference</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Choose your preference" menu={description_menu}/>
    </Grid>
    <Grid item xs={12} sm={12} lg={12}>
      <Typography variant="h5">Pricing rules</Typography>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Pricing Rules</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <ToggleButtons options={pricing_rules}  type="pricingrule"/>
    </Grid>
    {pricing_rules_selected === "ON"?(
      <>
      <Grid item xs={5} sm={5} lg={5}>
        <div>Mode</div>
      </Grid>
      <Grid item xs={5} sm={5} lg={5}>
        <ToggleButtons options={pricing_formula} type="pricingformula"/>
      </Grid>
      <Grid item xs={5} sm={5} lg={5}>
        <div>Product Price</div>
      </Grid>
      <Grid item xs={5} sm={5} lg={5}>
          <label>Product Cost + </label>
          <input style={{textAlign:"center", marginLeft:"1%"}} type="number" min="0" max="100"/>
          <label style={{textAlign:"center", marginLeft:"1%"}}>%</label>
      </Grid>
      </>
    ):("")}
    <Grid item xs={12} sm={12} lg={12}>
      <Typography variant="h5">Import Settings</Typography>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Ship from</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Choose a Country" menu={country_list_shipfrom} type="shipfrom"/>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Ship To</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Choose a Country" menu={country_list_shipto} type="shipto"/>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <div>Shipping Method</div>
    </Grid>
    <Grid item xs={5} sm={5} lg={5}>
      <BasicSelect label="Choose a shipping method" menu={shipping_methods} type="shippingmethod"/>
    </Grid>
    </>
    )
}

export default GeneralSettings;
