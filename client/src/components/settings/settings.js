import React, {useState, useEffect} from "react"

import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import LoginIcon from '@mui/icons-material/Login';
import Button from '@mui/material/Button';
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import BasicSelect from "../select/select"
import GeneralSettings from "../generalsettings/generalsettings"
import "./settings.css"
import {useSelector, useDispatch} from "react-redux"
import {setSearch, setArticles,setSubsettings} from "../../actions"
function Settings() {
    const [orders, setOrders] = useState("")
    const sub_settings = useSelector(state => state.sub_settings)
    const dispatch = useDispatch()
  return (
    <div style={{marginLeft:"5%", marginTop:"2%", marginRight:"5%"}}>

      <Grid container spacing={3}>
      <Grid item xs={12} sm={12} lg={12}>
        <Card>
        <Button className="settingsmenu">General</Button>
        <Button className="settingsmenu">Order Settings</Button>
        <Button className="settingsmenu" onClick={(event) => dispatch(setSubsettings("dropship"))}>Dropship Settings</Button>
        <Button className="settingsmenu">Listing Settings</Button>
        <Button className="settingsmenu">Company</Button>
        <Button className="settingsmenu">Advanced</Button>
        <Button className="settingsmenu">Integrations</Button>
        <Button className="settingsmenu">Account</Button>
        <Button className="settingsmenu">Billing</Button>
        <Divider/>
        </Card>
      </Grid>

        {(sub_settings === "dropship")?(
          <GeneralSettings/>
        ):""}

      </Grid>
    </div>

    )
}

export default Settings;
