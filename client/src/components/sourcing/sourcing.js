import React, {useState, useEffect} from "react"
import "./sourcing.css"
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import LoginIcon from '@mui/icons-material/Login';
import Button from '@mui/material/Button';
import ToggleButtons from "../togglebutton/togglebutton"
import {useSelector, useDispatch} from "react-redux"
import {setSearch, setArticles,setSubsettings, setToken, setCountrylist,setShippingMethods, setCategoriesLevel1, setCategoriesLevel2,setCategoriesLevel3, setCategoriesLevel4,setCategoriesLevel5, setCategoriesLevel6,setImportedProducts,setImportedProductsFilter} from "../../actions"
import BasicSelect from "../categoryselect/categoryselect"
import ProductInfo from "../sourcingproductinfo/sourcingproductinfo"

function Sourcing() {
  const import_options = useSelector(state => state.import_options)
  const selected_import_option = useSelector(state => state.selected_import_option)
  const categories_level1 = useSelector(state => state.categories_level1)
  const categories_level2 = useSelector(state => state.categories_level2)
  const categories_level3 = useSelector(state => state.categories_level3)
  const categories_level4 = useSelector(state => state.categories_level4)
  const categories_level5 = useSelector(state => state.categories_level5)
  const categories_level6 = useSelector(state => state.categories_level6)
  const selected_category_level2 = useSelector(state => state.selected_category_level2)
  const selected_category_level3 = useSelector(state => state.selected_category_level3)
  const selected_category_level4 = useSelector(state => state.selected_category_level4)
  const selected_category_level5 = useSelector(state => state.selected_category_level5)
  const selected_country_shipfrom = useSelector(state => state.selected_country_shipfrom)
  const selected_country_shipto = useSelector(state => state.selected_country_shipto)
  const imported_products_filter = useSelector(state => state.imported_products_filter)
  const imported_products = useSelector(state => state.imported_products)
  const token = useSelector(state => state.token)
  const dispatch = useDispatch()
  console.log(selected_import_option)

  useEffect(() => {
    const data = async () => {
      // await fetch("https://api.banggood.com/getAccessToken?app_id=bg614e8e254eca7&app_secret=5e76df83d25c08505022011c3d0f75f9", {headers:{"access-control-allow-origin" : "*"}}).then(response => response.json()).then(response => console.log(response))
      await fetch("http://localhost:3001/bangood").then(response => response.json()).then(response => dispatch(setToken(response)))
    }

      data()

  }, [])

  useEffect(() => {
    const data = async () => {

        let categories = []
        let categories_final = []

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:1})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:2})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:3})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:4})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:5})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:6})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:7})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

          await fetch("http://localhost:3001/bangoodproductcategories", {
            method:"POST",
            body: JSON.stringify({token:token.access_token, page:8})
          }).then(response => response.json()).then(response => {
              if(response.cat_list){
                categories.push(...response.cat_list)
              }
          })

        console.log(categories)
        if(categories.length === 3985){
          dispatch(setCategoriesLevel1(categories))
        }
      }
    data()
  }, [token])

useEffect(() => {
  let subcategories = []
  if(Array.isArray(categories_level1)){
    categories_level1.map(item => {
      if(item.parent_id === "0"){
        subcategories.push(item)
      }
    })
  }
console.log(subcategories)
dispatch(setCategoriesLevel2(subcategories))
},[categories_level1])

useEffect(() => {
  let subcategories = []

    categories_level1.map(item => {
      if(item.parent_id === selected_category_level2){
        subcategories.push(item)
      }
    })

console.log(subcategories)
dispatch(setCategoriesLevel3(subcategories))
},[selected_category_level2])

useEffect(() => {
  let subcategories = []

    categories_level1.map(item => {
      if(item.parent_id === selected_category_level3){
        subcategories.push(item)
      }
    })


dispatch(setCategoriesLevel4(subcategories))
},[selected_category_level3])

useEffect(() => {
  let subcategories = []

    categories_level1.map(item => {
      if(item.parent_id === selected_category_level4){
        subcategories.push(item)
      }
    })


dispatch(setCategoriesLevel5(subcategories))
},[selected_category_level4])

useEffect(() => {
  let subcategories = []

    categories_level1.map(item => {
      if(item.parent_id === selected_category_level5){
        subcategories.push(item)
      }
    })


dispatch(setCategoriesLevel6(subcategories))
},[selected_category_level5])

const handleImport = async () => {
  const items = []
  let array = []
  let filtered_array = []
  if(selected_category_level3 !== ""){
    await fetch("http://localhost:3001/bangoodproducts", {
      method:"POST",
      body: JSON.stringify({token:token.access_token, cat_id:selected_category_level3, page:1})
    }).then(response => response.json()).then(response => items.push(...response.product_list))

    await fetch("http://localhost:3001/bangoodproducts", {
      method:"POST",
      body: JSON.stringify({token:token.access_token, cat_id:selected_category_level3, page:2})
    }).then(response => response.json()).then(response => items.push(...response.product_list))

    await fetch("http://localhost:3001/bangoodproducts", {
      method:"POST",
      body: JSON.stringify({token:token.access_token, cat_id:selected_category_level3, page:3})
    }).then(response => response.json()).then(response => items.push(...response.product_list))


  console.log(items)

  items.map(item => {
    fetch("http://localhost:3001/bangoodproductinfo", {
      method:"POST",
      body: JSON.stringify({token:token.access_token, product_id:item.product_id})
    }).then(response => response.json()).then(response => {

        array = [...array, {...response, product_id:item.product_id}]
        dispatch(setImportedProducts(array))
    })

  })

}
}

useEffect(() => {
  const filtered_array = []
  let filtered_array_shipto = []
  if(imported_products.length === 60){
    imported_products.map(item => {
      item.warehouse_list.map(element => {
        if(element.warehouse === selected_country_shipfrom){
          filtered_array.push({...item})
        }
      })
    })
    console.log(filtered_array)
    filtered_array.map(item => {
      fetch("http://localhost:3001/shipping", {
        method:"POST",
        body: JSON.stringify({token:token.access_token, product_id:item.product_id, warehouse:selected_country_shipfrom, country:selected_country_shipto})
      }).then(response => response.json()).then(response => {
        console.log(response)
        if(response.shipmethod_list){
          item.shipment = response.shipmethod_list
          filtered_array_shipto = [...filtered_array_shipto, item]
          dispatch(setImportedProductsFilter(filtered_array_shipto))
        }
      })
    })
  }


}, [imported_products])

console.log(categories_level1,categories_level2, categories_level3, categories_level4, categories_level5, categories_level6)
console.log("imported_products", imported_products)
console.log("imported_products_filter", imported_products_filter)

  return (
    <Grid container style={{marginTop:"2%", marginLeft:"1%", marginRight:"1%"}}>
      <Grid style={{textAlign:"center"}} item xs={12} sm={12} lg={12}>
          <ToggleButtons options={import_options} type="importoption"/>
      </Grid>
      <Grid item xs={4} sm={4} lg={4} style={{marginTop:"2%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>
          <BasicSelect label="Choose a Category" menu={categories_level2} type="CATLEVEL2"/>

          </>
      ):("")}

      </Grid>
      <Grid item xs={4} sm={4} lg={4} style={{marginTop:"2%", marginLeft:"1%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>

          <BasicSelect label="Choose a SubCategory" menu={categories_level3} type="CATLEVEL3"/>

          </>
      ):("")}

      </Grid>
      <Grid item xs={3} sm={3} lg={3} style={{marginTop:"2%", marginLeft:"1%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>

          <BasicSelect label="Choose a SubCategory2" menu={categories_level4} type="CATLEVEL4"/>
          </>
      ):("")}

      </Grid>
      <Grid item xs={4} sm={4} lg={4} style={{marginTop:"2%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>

          <BasicSelect label="Choose a SubCategory3" menu={categories_level5} type="CATLEVEL5"/>
          </>
      ):("")}

      </Grid>
      <Grid item xs={4} sm={4} lg={4} style={{marginTop:"2%", marginLeft:"1%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>

          <BasicSelect label="Choose a SubCategory3" menu={categories_level6} type="CATLEVEL6"/>
          </>
      ):("")}

      </Grid>
      <Grid item xs={3} sm={3} lg={3} style={{marginTop:"2%", marginLeft:"5%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) ?(
          <>
        <Button onClick={(event) => handleImport(event)} style={{height:"100%"}} variant="contained">IMPORT PRODUCTS</Button>
        </>
      ):("")
    }
      </Grid>
      <Grid item xs={12} sm={12} lg={12} style={{marginTop:"2%", marginLeft:"5%"}}>
      {selected_import_option === "Import by Category" && Array.isArray(categories_level2) && Array.isArray(imported_products_filter)?(
        <>
        {
          imported_products_filter.map(item => {
          return (
            <>
            <p>{item.product_id}</p>
            <p>{item.product_name}</p>
            </>
          )
        })
      }
        </>
      ):("")
    }
      </Grid>

    </Grid>

    )
}

export default Sourcing;
