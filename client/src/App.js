import logo from './logo.svg';
import React, {useState, useEffect} from "react"
import './App.css';
import axios from 'axios';
import AppBar from "./components/navbar/navbar"
import ShopifyOrders from "./components/shopifyorders/shopifyorders"
import Login from "./components/login/login"
import SupplierProducts from "./components/supplierproducts/supplierproducts"
import ShopifyProducts from "./components/shopifyproducts/shopifyproducts"
import ProductInfo from "./components/productinfo/productinfo"
import PrimarySearchAppBar from "./components/appbar/appbar"
import Settings from "./components/settings/settings"
import Sourcing from "./components/sourcing/sourcing"
import {useSelector, useDispatch} from "react-redux"

function App({store}) {
  const admin = {username:"info@paralleldeals.com", password:"salespro@2021"}
  const [page, setPage] = useState("")
  const [logged, setLogged] = useState(true)
  const [adminuser, setAdminuser] = useState("")
  const [adminpassword, setAdminpassword] = useState("")

  const handlePage = (event, id) => {
    setPage(id)
  }

  const handleLogin = (event) => {
    if(adminuser === admin.username && adminpassword === admin.password){
      setLogged(true)
    }else{
      alert("Login details are Incorrect, Please contact the Administrator")
    }
  }
  const handleLogout = (event) => {
    setLogged(false)
    setAdminuser("")
    setAdminpassword("")
  }

  const handleAdminUser = (event, id) => {
    if(id === "user"){
      setAdminuser(event.target.value)
    }
    if(id === "pass"){
      setAdminpassword(event.target.value)
    }
  }


  console.log(page)
  return (

    <div className="mainpage">

    {
      (logged === false)?(
        <div className="">
      <Login handleLogin={handleLogin} handleAdminUser={handleAdminUser} />
      </div>
    ):(
      <>
      <AppBar classname="appbar" handlePage={handlePage} handleLogout={handleLogout}/>
      <PrimarySearchAppBar handlePage={handlePage}/>

        {
        (page === "settings")?(
          <Settings/>
        ):""
        }
        {
        (page === "shopifyorders")?(
          <ShopifyOrders/>
        ):""
        }
        {
        (page === "supplierproducts")?(
          <SupplierProducts/>
        ):""
        }
        {
        (page === "shopifyproducts")?(
          <ShopifyProducts/>
        ):""
        }
        {
        (page === "sourcing")?(
          <Sourcing/>
        ):""
        }

        </>
    )
    }



    </div>
  );
}

export default App;
