
import fetch from "node-fetch"
import express from "express"
import cors from "cors"
import bodyparser from "body-parser"
import Shopify from "shopify-api-node"
const app = express()

const shopify = new Shopify({
     shopName: "goslash-au.myshopify.com",
     accessToken: "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",
   });


app.use(bodyparser.json({ type: '*/*' }))
app.use(cors())

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get("/bangood", cors(), (req,res) => {
  fetch("https://api.banggood.com/getAccessToken?app_id=bg614e8e254eca7&app_secret=5e76df83d25c08505022011c3d0f75f9").then(response => response.json()).then(response => res.json(response))
})

app.post("/bangoodproductcategories", cors(), (req,res) => {

  fetch(`https://api.banggood.com/category/getCategoryList?access_token=${req.body.token}&page=${req.body.page}`).then(response => response.json()).then(response => res.json(response))
})

app.post("/bangoodcountries", cors(), (req,res) => {
  console.log(req.body.token)
  fetch(`https://api.banggood.com/category/getCountries?access_token=${req.body.token}`).then(response => response.json()).then(response => res.json(response))
})

app.post("/getstock", cors(), (req,res) => {
  console.log(req.body.token)
  fetch(`https://api.banggood.com/product/getStocks?product_id=${req.body.product_id}&access_token=${req.body.token}&`).then(response => response.json()).then(response => res.json(response))
})

app.post("/bangoodproducts", cors(), (req,res) => {

  fetch(`https://api.banggood.com/product/getProductList?cat_id=${req.body.cat_id}&access_token=${req.body.token}&page=${req.body.page}`).then(response => response.json()).then(response => res.json(response))
})

app.post("/bangoodproductinfo", cors(), (req,res) => {

  fetch(`https://api.banggood.com/product/getProductInfo?product_id=${req.body.product_id}&access_token=${req.body.token}`).then(response => response.json()).then(response => res.json(response))
})

app.post("/shipping", cors(), (req,res) => {

  fetch(`https://api.banggood.com/product/getShipments?product_id=${req.body.product_id}&access_token=${req.body.token}&warehouse=${req.body.warehouse}&country=${req.body.country}`).then(response => response.json()).then(response => res.json(response))
})




app.post("/addproduct", cors(), (req,res) => {
    // const product = await shopify.product.create(JSON.stringify(req.body))

  // fetch("https://goslash-au.myshopify.com/admin/api/2021-07/products.json", {
  //     method: 'post',
  //     headers:{'X-Shopify-Access-Token':'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', 'Accept': 'application/json'},
  //     body: JSON.stringify(req.body)
  //   }).then(response => response.json()).then(response => res.json(response))

  shopify.product.create(req.body.product)
    .then((product) => res.json(product))
    .catch((err) => console.error(err))
})

app.post("/updatevariant", cors(), (req,res) => {
    // const product = await shopify.product.create(JSON.stringify(req.body))

  // fetch("https://goslash-au.myshopify.com/admin/api/2021-07/products.json", {
  //     method: 'post',
  //     headers:{'X-Shopify-Access-Token':'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', 'Accept': 'application/json'},
  //     body: JSON.stringify(req.body)
  //   }).then(response => response.json()).then(response => res.json(response))

  shopify.productVariant.update(req.body.id, req.body.params)
    .then((variant) => res.json(variant))
    .catch((err) => console.error(err))
})


app.get("/shopifyorders", cors(), (req,res) => {
   fetch("https://goslash-au.myshopify.com/admin/api/2021-07/orders.json?limit=250",   {
      method: 'GET',
      headers:{'X-Shopify-Access-Token':'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'}}).then(response => response.json()).then(response => res.json(response))
})

app.get("/shopifyproducts", cors(), (req,res) => {
  fetch("https://goslash-au.myshopify.com/admin/api/2021-07/products.json?limit=250&vendor=BG03AU",   {
      method: 'GET',
      headers:{'X-Shopify-Access-Token':'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'}}).then(response => response.json()).then(response => res.json(response))
})



app.listen(3001, () => {
  console.log("server is up and running")
})
